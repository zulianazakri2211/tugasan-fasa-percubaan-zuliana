<!DOCTYPE html>
<html>
<head>
  <title>Electricity Calculator</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    .container {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>Electricity Calculator</h1>
    <form method="post" action="">
      <div>
        <label for="voltage">Voltage (V):</label>
        <input type="text" id="voltage" name="voltage" required pattern="\d+(\.\d+)?" title="Please enter a decimal number" />
      </div>
      <div>
        <label for="current">Current (A):</label>
        <input type="text" id="current" name="current" required pattern="\d+(\.\d+)?" title="Please enter a decimal number" />
      </div>
      <div>
        <input type="submit" value="Calculate">
      </div>
    </form>

    <?php
      function calculateElectricityRate($voltage, $current, $rate) {
        $power = $voltage * $current / 1000; // Convert power to kilowatts
        $energy = $power * 24; // Energy within 24 hours in kilowatt-hours
        $total = 0;

        if ($energy <= 200) {
          $total = $energy * 21.80;
          $rate = 21.80;
        } elseif ($energy <= 300) {
          $total = (200 * 21.80) + (($energy - 200) * 33.40);
          $rate = 33.40;
        } elseif ($energy <= 600) {
          $total = (200 * 21.80) + (100 * 33.40) + (($energy - 300) * 51.60);
          $rate = 51.60;
        } elseif ($energy <= 900) {
          $total = (200 * 21.80) + (100 * 33.40) + (300 * 51.60) + (($energy - 600) * 54.60);
          $rate = 54.60;
        } else {
          $total = (200 * 21.80) + (100 * 33.40) + (300 * 51.60) + (300 * 54.60) + (($energy - 900) * 57.10);
          $rate = 57.10;
        }

        return [
          'total' => $total,
          'power' => $power,
          'energy' => $energy,
          'rate' => $rate
        ];
      }

      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $voltage = $_POST["voltage"];
        $current = $_POST["current"];

        $rate = isset($_POST["rate"]) ? $_POST["rate"] : 0;
        $result = calculateElectricityRate($voltage, $current, $rate);
        $total = $result['total'];
        $power = $result['power'];
        $energy = $result['energy'];
        $rate = $result['rate'];

        echo '<div class="result">';
        echo '<h2>Results:</h2>';
        echo '<p class="output-line">Power: ' . $power . ' kW</p>';
        echo '<p class="output-line">Energy: ' . $energy . ' kWh</p>';
        echo '<p class="output-line">Current Rate: RM ' . number_format($rate, 2, '.', '') . '</p>';
        echo '<p class="output-line">Total Charge (24 hours): RM ' . number_format($total, 2) . '</p>';
        echo '</div>';
      }
      ?>

  </div>
</body>
</html>